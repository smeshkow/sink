.PHONY: setup fmt test test-race

TAG?=$(shell git describe --abbrev=0 --tags)

export TAG

all: lint test

version:
	echo $(TAG)

install:
	go get .

lint: install
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install
	gometalinter --fast --vendor ./...

fmt:
	gofmt -w=true -s $$(find . -type f -name '*.go' -not -path "./vendor/*")

test: fmt
	go test -v ./...

test-race:
	go test -race -v ./...
