# Sink #

Collection of abstractions for logging and metrics with default implementations.

## Usage

See [main.go](https://bitbucket.org/atlassian/sink/src/master/main.go).

## Changelog

See [CHANGELOG.md](https://bitbucket.org/atlassian/sink/src/master/CHANGELOG.md).

## Contributing

See [CONTRIBUTING.md](https://bitbucket.org/atlassian/sink/src/master/CONTRIBUTING.md).
