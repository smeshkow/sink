# Changelog
All notable changes to this project will be documented in this file.

### 0.4.0
 - moved onto Go Modules.

### 0.3.0
 - moved project under atlassian owner.

### 0.2.0
 - added `logger.Logger.Fatal` method to support fatal cases, which require exit with code 1 after logging error.

### 0.1.1
 - added `main.go` with examples;
 - fixed bug with logger not handling variadic arguments properly;
 - updated README.md.

### 0.1.0
 - first release
