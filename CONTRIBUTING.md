# Contributing

## Contributor License Agreement

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate link below to digitally sign the CLA. The Corporate CLA is for those who are contributing as a member of an organisation and the individual CLA is for those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## Guidelines for pull requests

- Write tests for any changes.
- Separate unrelated changes into multiple pull requests.
- For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.
- Ensure the build is green before you open your PR. The Pipelines build won't run by default on a remote branch, so enable Pipelines.
- Update [CHANGELOG.md](https://bitbucket.org/atlassian/sink/raw/master/CHANGELOG.md) with new version and explanation for added functionality/fix.
- We support [SemVer 2.0](http://semver.org/spec/v2.0.0.html).
- Running `_bin/tag x.y.z` will push desired version, so it'll be available for consumption.

## Development dependencies

- [Go](https://golang.org/dl/) 1.11+

## Setting up a development machine

Run `make`.

## During development

- `make` - Runs all the tests and lints code. Execute it before pushing any changes.

## Write tests

- please read [Go advanced testing tips & tricks](https://medium.com/@povilasve/go-advanced-tips-tricks-a872503ac859) 1st.
- unit tests follow the default Go's pattern 
- test helpers have `*_testing.go` suffix in the file names, so they available for any package in case it wants to use these helpers too
- integration tests have following suffixes `*_integration_test.go`, plus `// +build integration` 
build tag comment at the top of the file in order to separate them from the unit tests